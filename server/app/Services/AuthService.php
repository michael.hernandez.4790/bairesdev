<?php

namespace App\Services;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

/**
 * Auth Service
 */
class AuthService
{
    /**
     * Register new user
     */
    public function register(string $name, string $email, string $password) : User
    {
        return User::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);
    }

    public function login(string $email, string $password, bool $remember = false)
    {
        if (!Auth::attempt(compact('email', 'password'))) {
            throw new Exception('Unauthorized', 401);
        }

        $user = request()->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($remember) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        $token->save();

        return compact('token', 'tokenResult');
    }

    public function logout()
    {
        request()->user()->token()->revoke();
    }
}