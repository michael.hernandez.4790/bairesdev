<?php

namespace App\Services\Actions;

use App\Events\AircraftCreated;
use App\Jobs\ProcessAircraftJob;
use App\Models\Aircraft;

/**
 * Enqueue Aircraft Action
 */
class EnqueueAircraftAction
{
    public function execute(int $type, int $size) : Aircraft
    {
        $model = new Aircraft();
        $model->type_id = $type;
        $model->size_id = $size;
        $model->save();

        event(new AircraftCreated($model));
        ProcessAircraftJob::dispatch($model->id);

        return $model;
    }
}