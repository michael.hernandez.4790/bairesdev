<?php

namespace App\Services\Actions;

use App\Events\AircraftProcessed;
use App\Models\Aircraft;

/**
 * Dequeue Aircraft Action
 */
class DequeueAircraftAction
{
    /**
     * Dequeue an aircraft, if there is one on the list, will dequeue and return it its ID
     * if the list is empty return 0
     *
     * @param int $id - dequeue current id (optional)
     */
    public function execute(int $id = null) : int
    {
        if (!empty($id)) {
            $aircraft = Aircraft::pending()->findOrFail($id);
        } else {
            // getting next pending aircraft on the queue
            $aircraft = Aircraft::pending()->first();
        }

        if ($aircraft) {
            $aircraft->processed = 1;
            $aircraft->save();

            event(new AircraftProcessed($aircraft));
            return $aircraft->id;
        }

        return 0;
    }
}