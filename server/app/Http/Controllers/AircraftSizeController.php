<?php

namespace App\Http\Controllers;

use App\Http\Resources\AircraftSizeCollection;
use App\Models\AircraftSize;

class AircraftSizeController extends Controller
{
    public function index()
    {
        return new AircraftSizeCollection(AircraftSize::orderByDesc('weight')->get());
    }
}
