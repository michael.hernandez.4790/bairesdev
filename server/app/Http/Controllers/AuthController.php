<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Services\AuthService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Registro de usuario
     */
    public function signUp(UserRegisterRequest $request, AuthService $service)
    {
        $service->register($request->name, $request->email, $request->password);

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(UserLoginRequest $request, AuthService $service)
    {
        try {
            extract($service->login($request->email, $request->password, (bool) $request->remember));

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
            ]);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 401);
        }
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request, AuthService $service)
    {
        $service->logout();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        return response()->json(['data' => $request->user()]);
    }
}
