<?php

namespace App\Http\Controllers;

use App\Http\Resources\AircraftTypeCollection;
use App\Models\AircraftType;

class AircraftTypeController extends Controller
{
    public function index()
    {
        return new AircraftTypeCollection(AircraftType::orderByDesc('weight')->get());
    }
}
