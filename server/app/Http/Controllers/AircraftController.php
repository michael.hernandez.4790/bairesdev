<?php

namespace App\Http\Controllers;

use App\Http\Requests\AircraftPostRequest;
use App\Http\Resources\AircraftCollection;
use App\Http\Resources\AircraftResource;
use App\Models\Aircraft;
use App\Services\Actions\EnqueueAircraftAction;
use Illuminate\Http\Request;

class AircraftController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'per_page' => 'numeric|min:1|max:100'
        ]);

        return new AircraftCollection(Aircraft::withoutGlobalScope('sorted')
            ->orderByDesc('id')
            ->paginate($request->input('per_page', 10)));
    }

    public function store(AircraftPostRequest $request, EnqueueAircraftAction $action)
    {
        $type = $request->input('type');
        $size = $request->input('size');

        $model = $action->execute($type, $size);

        return new AircraftResource($model);
    }
}
