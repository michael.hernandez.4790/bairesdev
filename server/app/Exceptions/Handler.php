<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\PostTooLargeException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }


    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        //if ($request->expectsJson()) {
            if ($exception instanceof PostTooLargeException) {
                return response()->json(
                    [
                        'message' => "Size of attached file should be less " . ini_get("upload_max_filesize") . "B"
                    ],
                    400
                );
            }

            if ($exception instanceof AuthenticationException) {
                return response()->json(
                    [
                        'message' => 'Unauthenticated or Token Expired, Please Login'
                    ],
                    401
                );
            }

            if ($exception instanceof ThrottleRequestsException) {
                return response()->json(
                    [
                        'message' => 'Too Many Requests,Please Slow Down'
                    ],
                    429
                );
            }

            if ($exception instanceof ModelNotFoundException) {
                return response()->json(
                    [
                        'message' => 'Entry for ' . str_replace('App\\', '', $exception->getModel()) . ' not found'
                    ],
                    404
                );
            }

            if ($exception instanceof ValidationException) {
                return response()->json(
                    [
                        'message' => $exception->getMessage(),
                        'errors' => $exception->errors()
                    ],
                    422
                );
            }

            if ($exception instanceof QueryException) {
                return response()->json(
                    [
                        'message' => 'There was Issue with the Query',
                        'exception' => $exception

                    ],
                    500
                );
            }

            if ($exception instanceof \Error) {
                return response()->json(
                    [
                        'message' => "There was some internal error",
                        'exception' => $exception
                    ],
                    500
                );
            }
        //}

        return parent::render($request, $exception);
    }
}
