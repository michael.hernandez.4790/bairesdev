<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Aircraft extends Model
{
    use HasFactory;

    protected $table = 'aircrafts';

    public $fillable = [
        'type_id',
        'size_id',
        'sort',
        'processed',
    ];

    /**
     * The "boot" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Adding the sort (weight) that could have this aircraft
        // it will sort desc by this field
        static::creating(function ($aircraft) {
            $aircraft->sort = $aircraft->type->weight * 100 + $aircraft->size->weight;
        });
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('sorted', function (Builder $builder) {
            $builder->orderByDesc('sort');
            $builder->orderBy('id');
        });
    }

    /**
     * Get the type model.
     */
    public function type()
    {
        return $this->belongsTo(AircraftType::class, 'type_id');
    }

    /**
     * Get the size model.
     */
    public function size()
    {
        return $this->belongsTo(AircraftSize::class, 'size_id');
    }

    /**
     * Scope a query to only include pendings aircrafts.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->where('processed', 0);
    }
}
