<?php

namespace Database\Factories;

use App\Models\AircraftType;
use Illuminate\Database\Eloquent\Factories\Factory;

class AircraftTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AircraftType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->title(),
            'weight' => $this->faker->randomDigitNotNull(),
        ];
    }
}
