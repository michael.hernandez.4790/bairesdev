<?php

namespace Database\Factories;

use App\Models\AircraftSize;
use Illuminate\Database\Eloquent\Factories\Factory;

class AircraftSizeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AircraftSize::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->title(),
            'weight' => $this->faker->randomDigitNotNull(),
        ];
    }
}
