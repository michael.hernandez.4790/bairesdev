<?php

namespace Database\Factories;

use App\Models\Aircraft;
use App\Models\AircraftSize;
use App\Models\AircraftType;
use Illuminate\Database\Eloquent\Factories\Factory;

class AircraftFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Aircraft::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type_id' => AircraftType::factory(),
            'size_id' => AircraftSize::factory(),
            'processed' => 0
        ];
    }
}
