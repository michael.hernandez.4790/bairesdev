<?php

namespace Database\Seeders;

use App\Models\AircraftType;
use Illuminate\Database\Seeder;

class AircraftTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AircraftType::create([
            'name' => 'Emergency',
            'weight' => 4
        ]);

        AircraftType::create([
            'name' => 'VIP',
            'weight' => 3
        ]);

        AircraftType::create([
            'name' => 'Passenger',
            'weight' => 2
        ]);

        AircraftType::create([
            'name' => 'Cargo',
            'weight' => 1
        ]);
    }
}
