<?php

namespace Database\Seeders;

use App\Models\AircraftSize;
use Illuminate\Database\Seeder;

class AircraftSizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AircraftSize::create([
            'name' => 'large',
            'weight' => 1
        ]);

        AircraftSize::create([
            'name' => 'Small',
            'weight' => 0
        ]);
    }
}
