<?php

use App\Http\Controllers\AircraftController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AircraftTypeController;
use App\Http\Controllers\AircraftSizeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('signup', [AuthController::class, 'signUp']);

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('user', [AuthController::class, 'user']);
    });
});

Route::group([
    'middleware' => 'auth:api'
  ], function() {
      Route::get('types', [AircraftTypeController::class, 'index']);
      Route::get('sizes', [AircraftSizeController::class, 'index']);
      Route::post('aircrafts', [AircraftController::class, 'store']);
      Route::get('aircrafts', [AircraftController::class, 'index']);
});

Route::fallback(function(){
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact info@website.com'
    ], 404);
});