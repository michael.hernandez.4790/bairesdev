<?php

namespace Tests\Feature;

use App\Models\Aircraft;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AircraftTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateEntitySuccess()
    {
        Passport::actingAs(User::factory()->create());
        $response = $this->json('POST', '/api/aircrafts', [
            'type' => 1,
            'size' => 1
        ]);

        $response->assertStatus(201);
    }

    public function testCreateEntityError()
    {
        Passport::actingAs(User::factory()->create());
        $response = $this->json('POST', '/api/aircrafts', [
            'type' => 100,
            'size' => 100
        ]);

        $response->assertStatus(422);
    }

    public function testListEntities()
    {
        Aircraft::factory()->create();
        Aircraft::factory()->create();
        Aircraft::factory()->create();

        Passport::actingAs(User::factory()->create());
        $response = $this->json('GET', '/api/aircrafts');

        $response
            ->assertStatus(200)
            ->assertJsonCount(3, 'data');
    }
}
