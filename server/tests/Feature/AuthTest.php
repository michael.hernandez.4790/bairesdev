<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateUserSuccess()
    {
        $response = $this->json('POST', '/api/auth/signup', [
            'name' => 'name test',
            'email' => 'email@test.com',
            'password' => '123456',
        ]);

        $response->assertStatus(201);
    }

    public function testCreateDuplicateUser()
    {
        $user = User::factory()->create();
        $response = $this->json('POST', '/api/auth/signup', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => $user->password,
        ]);

        $response->assertStatus(422);
    }

    public function testCallAnyRouteLogged()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $response = $this->json('GET', '/api/aircrafts');

        $response->assertStatus(200);
    }
}
