<?php

namespace Tests\Unit;

use App\Services\Actions\EnqueueAircraftAction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EnqueueAircraftActionTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    protected $action;

    protected function setUp() : void {
        parent::setUp();
        $this->action = resolve(EnqueueAircraftAction::class);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSuccessInsert()
    {
        $aircraft = $this->action->execute(1, 1);
        $this->assertEquals(401, $aircraft->sort);
    }

    public function testMultipleInsert()
    {
        $types = [
            // ID => WEIGHT
            1 => 4,
            2 => 3,
            3 => 2,
            4 => 1,
        ];

        $sizes = [
            // ID => WEIGHT
            1 => 1,
            2 => 0,
        ];

        $quantity = rand(0, 50);
        for ($i=0; $i < $quantity; $i++) {
            $typeId = rand(1,4);
            $sizeId = rand(1,2);

            $aircraft = $this->action->execute($typeId, $sizeId);
            $this->assertEquals($types[$typeId] * 100 + $sizes[$sizeId], $aircraft->sort);
        }
    }
}
