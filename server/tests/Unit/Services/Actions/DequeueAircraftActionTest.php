<?php

namespace Tests\Unit;

use App\Jobs\ProcessAircraftJob;
use App\Services\Actions\DequeueAircraftAction;
use App\Services\Actions\EnqueueAircraftAction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;

class DequeueAircraftActionTest extends TestCase
{
    use RefreshDatabase;

    protected $seed = true;

    protected $enqueueAction;
    protected $dequeueAction;

    protected function setUp() : void {
        parent::setUp();
        $this->enqueueAction = resolve(EnqueueAircraftAction::class);
        $this->dequeueAction = resolve(DequeueAircraftAction::class);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSuccessDequeue()
    {
        Bus::fake();
        $aircraft = $this->enqueueAction->execute(1, 1);
        Bus::assertDispatched(ProcessAircraftJob::class);
        $this->assertEquals($aircraft->id, $this->dequeueAction->execute());
    }

    public function testSuccessDequeuePriorityAircraft()
    {
        Bus::fake();
        $aircraft1 = $this->enqueueAction->execute(2, 1);
        // this is priority
        $aircraft2 = $this->enqueueAction->execute(1, 1);
        $aircraft3 = $this->enqueueAction->execute(3, 1);

        $this->assertEquals($aircraft2->id, $this->dequeueAction->execute());
    }

    public function testSuccessDequeuePriorityAircraft2()
    {
        Bus::fake();
        $aircraft1 = $this->enqueueAction->execute(2, 1);
        // this is priority because was inserted before
        $aircraft2 = $this->enqueueAction->execute(1, 1);
        $aircraft3 = $this->enqueueAction->execute(1, 1);

        $this->assertEquals($aircraft2->id, $this->dequeueAction->execute());
    }

    public function testEmptyDequeue()
    {
        Bus::fake();
        $this->assertEquals(0, $this->dequeueAction->execute());
    }
}
