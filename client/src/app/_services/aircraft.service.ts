import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Aircraft } from '../_class/aircraft';
import Pusher from 'pusher-js';

@Injectable({
  providedIn: 'root'
})
export class AircraftService {

  private subjectProcessed: Subject<Aircraft> = new Subject<Aircraft>();
  private subjectCreated: Subject<Aircraft> = new Subject<Aircraft>();
  private pusherClient: Pusher;

  constructor() {
    this.pusherClient = new Pusher('bbe869c2a01c20ff66b9', { cluster: 'us2' });

    const channel = this.pusherClient.subscribe('aircrafts');

    channel.bind(
      'aircraft.processed',
      (data: any) => {
        this.subjectProcessed.next(new Aircraft(
          data.id,
          data.type,
          data.size,
          data.processed,
          new Date(data.created_at),
          new Date(data.updated_at)
        ));
      }
    );

    channel.bind(
      'aircraft.created',
      (data: any) => {
        this.subjectCreated.next(new Aircraft(
          data.id,
          data.type,
          data.size,
          data.processed,
          new Date(data.created_at),
          new Date(data.updated_at)
        ));
      }
    );
  }

  getProcessed(): Observable<Aircraft> {
    return this.subjectProcessed.asObservable();
  }

  getCreated(): Observable<Aircraft> {
    return this.subjectCreated.asObservable();
  }
}
