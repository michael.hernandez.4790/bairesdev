import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private REST_API_SERVER = "http://localhost:8000/api";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getAircraftTypes(){
    return this.httpClient.get(`${this.REST_API_SERVER}/types`).pipe(catchError(this.handleError));
  }

  public getAircraftSizes(){
    return this.httpClient.get(`${this.REST_API_SERVER}/sizes`).pipe(catchError(this.handleError));
  }

  public getAircrafts(page: number = 1, perPage: number = 10){
    return this.httpClient.get(`${this.REST_API_SERVER}/aircrafts`, {
      params: {
        page,
        'per_page': perPage
      }
    }).pipe(catchError(this.handleError));
  }

  public setAircraft(data: any){
    return this.httpClient.post(`${this.REST_API_SERVER}/aircrafts`, data).pipe(catchError(this.handleError));
  }
}
