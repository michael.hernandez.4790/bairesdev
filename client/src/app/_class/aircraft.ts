export class Aircraft {

    public highlight: boolean = false;

    constructor(
        public id: number,
        public type: string,
        public size: string,
        public processed: boolean,
        public createdAt: Date,
        public updatedAt: Date
    ) {
        this.id = id;
        this.type = type;
        this.size = size;
        this.processed = processed;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }
}
