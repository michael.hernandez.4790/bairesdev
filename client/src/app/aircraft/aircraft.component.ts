import { Component, OnInit, OnDestroy  } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { DataService } from '../_services/data.service';
import { Subscription } from 'rxjs';
import { AircraftService } from '../_services/aircraft.service';
import { Aircraft } from '../_class/aircraft';

interface Dropdown {
  id: number;
  name: string;
}

@Component({
  selector: 'app-aircraft',
  templateUrl: './aircraft.component.html',
  styleUrls: ['./aircraft.component.scss']
})
export class AircraftComponent implements OnInit, OnDestroy {
  types: Dropdown[] = [];
  sizes: Dropdown[] = [];
  aircrafts: Aircraft[] = [];
  pagination: any = {};
  perPage: number = 10;
  private aircraftSubscriptionProcessed: Subscription;
  private aircraftSubscriptionCreated: Subscription;

  aircraftForm = this.formBuilder.group({
    type: ['', Validators.required],
    size: ['', Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private aircraftService: AircraftService
  ) {
    this.aircraftSubscriptionProcessed = aircraftService
      .getProcessed()
      .subscribe((aircraft: Aircraft) => {
        let index = this.aircrafts.findIndex(item => item.id === aircraft.id);
        if (index >= 0) {
          aircraft.highlight = true;
          this.aircrafts[index] = aircraft;
        }
      });


    this.aircraftSubscriptionCreated = aircraftService
      .getCreated()
      .subscribe((aircraft: Aircraft) => {
        this.aircrafts.unshift(aircraft)
        this.aircrafts = this.aircrafts.slice(0, this.perPage);
      });
  }

  ngOnInit(): void {
    this.dataService.getAircraftTypes().subscribe((data: any) => {
      this.types = data.data;
    });

    this.dataService.getAircraftSizes().subscribe((data: any) => {
      this.sizes = data.data;
    });

    this.fetchAircrafts();
  }

  fetchAircrafts(page: number = 1) {
    this.dataService.getAircrafts(page, this.perPage).subscribe((data: any) => {
      this.aircrafts = data.data.map((response: any) => new Aircraft(
        response.id,
        response.type,
        response.size,
        response.processed,
        new Date(response.created_at),
        new Date(response.updated_at)
      ));
      this.pagination = data.meta;
    });
  }

  onSubmit(): void {
    this.dataService.setAircraft(this.aircraftForm.value).subscribe();
    // this.aircraftForm.reset();
  }

  onPerPageChange(perPage: number): void {
    this.perPage = perPage;
    this.fetchAircrafts(1);
  }

  onPageChange(page: number) : void {
    this.fetchAircrafts(page);
  }

  ngOnDestroy() {
    this.aircraftSubscriptionProcessed.unsubscribe();
    this.aircraftSubscriptionCreated.unsubscribe();
  }
}
