# **Air Traffic Control system**
There are 2 projects: frontend (Angular) and Backend (Laravel)

## Pre-requires
 - docker installed
 - ng installed

## Backend
For backend is necessary run Docker (at least you have all dependencies installed in your machine)

```docker-compose build```

```docker-compose up -d```

Create an *.env* file

```cd server```

```cp .env.example .env```

Go to the app container

```docker exec -it bairesdev-app bash```

Inside app container run the next commands

```composer install```

```php artisan migrate```

```php artisan db:seed```

```php artisan passport:install```

Run the queue

```php artisan queue:work redis```

This will mantein the queue running *DONT CLOSE THE TERMINAL*

the endpoint will be http://localhost:8000

## Frontend
In a new terminal, run the next steps:

```cd client```

```npm install```

```ng serve --open```

the frontend will run http://localhost:4200

This will mantein the frontend running *DONT CLOSE THE TERMINAL*


